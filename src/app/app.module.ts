import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { UsersComponent } from './components/users/users.component';
import { SidebarComponent } from './components/users/sidebar/sidebar.component';
import { TopbarComponent } from './components/users/topbar/topbar.component';
import { FormComponent } from './components/users/form/form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserComponent } from './components/users/sidebar/user/user.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { SidebarSearchInputPipe } from './components/users/sidebar/sidebar-search-input.pipe';
import { FormDataPipe } from './components/users/form/form-data.pipe';
import { LoginComponent } from './components/login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { UserFormStyleDirective } from './components/users/form/user-form-style.directive';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'users', component: UsersComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    SidebarComponent,
    TopbarComponent,
    FormComponent,
    UserComponent,
    SidebarSearchInputPipe,
    FormDataPipe,
    LoginComponent,
    UserFormStyleDirective,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    MatIconModule,
    BrowserAnimationsModule,
    FormsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
