import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formDataPipe'
})
export class FormDataPipe implements PipeTransform {

  transform(value: number | Date, action:string, extraValue?: number): number | string {
    if(action==="contactToVisual"){
      return "+"+extraValue+" "+value;
    }
    else if(action==="inchesToMeters" && typeof value === 'number'){
      return value * 0.0254;
    }
    return "";
  }

}
