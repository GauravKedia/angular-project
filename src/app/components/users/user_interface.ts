export interface User {
    username: string;
    age: number; // Will calculate this on basis of dateOfBirth
    gender: string;
    location: string;
    profilePhoto: string;
    userType: string;
    dateOfBirth: Date;
    contact: number; // Phone Number of 10 digit only
    countryCode: number; // Store only number add + to display
    height: number; // Storing In inches
  }
  


// Exercises (additions to last user list project):
// user type - differentiate styling and display/hide properties based on type
// height - store values in inches, convert to m, use number pipe to restrict to 2 decimal places
// contact - store country and add country code to contact for display
// canDelete, canEdit - show/hide edit, delete buttons based on values
// date of birth - apply date pipe
// Search term filter - custom pipe 
// move delete and edit buttons to user list
// input validations - required