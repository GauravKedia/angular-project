import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { User } from './user_interface';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  baseProfilePath: string = "assets/user_images/";
  users: User[] = [
    {
      username: 'John Doe1',
      age: 30,
      gender: 'Male',
      location: 'New York',
      profilePhoto: 'assets/user_images/boy.png',
      userType: 'Admin',
      dateOfBirth: new Date('1992-05-15'),
      contact: 1234567890,
      countryCode: 1,
      height: 72
    },
    {
      username: 'Jane Smith1',
      age: 25,
      gender: 'Female',
      location: 'Los Angeles',
      profilePhoto: 'assets/user_images/woman.png',
      userType: 'User',
      dateOfBirth: new Date('1997-08-20'),
      contact: 9876543210,
      countryCode: 91,
      height: 65
    },
    {
      username: 'Jane Smith2',
      age: 25,
      gender: 'Female',
      location: 'Los Angeles',
      profilePhoto: 'assets/user_images/woman.png',
      userType: 'User',
      dateOfBirth: new Date('1997-08-20'),
      contact: 9876543210,
      countryCode: 91,
      height: 65
    },{
      username: 'Jane Smith3',
      age: 25,
      gender: 'Female',
      location: 'Los Angeles',
      profilePhoto: 'assets/user_images/woman.png',
      userType: 'User',
      dateOfBirth: new Date('1997-08-20'),
      contact: 9876543210,
      countryCode: 91,
      height: 65
    },{
      username: 'Jane Smith4',
      age: 25,
      gender: 'Female',
      location: 'Los Angeles',
      profilePhoto: 'assets/user_images/woman.png',
      userType: 'User',
      dateOfBirth: new Date('1997-08-20'),
      contact: 9876543210,
      countryCode: 91,
      height: 65
    },{
      username: 'Jane Smith5',
      age: 25,
      gender: 'Female',
      location: 'Los Angeles',
      profilePhoto: 'assets/user_images/woman.png',
      userType: 'User',
      dateOfBirth: new Date('1997-08-20'),
      contact: 9876543210,
      countryCode: 91,
      height: 65
    },{
      username: 'Jane Smith6',
      age: 25,
      gender: 'Female',
      location: 'Los Angeles',
      profilePhoto: 'assets/user_images/woman.png',
      userType: 'User',
      dateOfBirth: new Date('1997-08-20'),
      contact: 9876543210,
      countryCode: 91,
      height: 65
    },{
      username: 'Jane Smith7',
      age: 25,
      gender: 'Female',
      location: 'Los Angeles',
      profilePhoto: 'assets/user_images/woman.png',
      userType: 'User',
      dateOfBirth: new Date('1997-08-20'),
      contact: 9876543210,
      countryCode: 91,
      height: 65
    },{
      username: 'Jane Smith',
      age: 25,
      gender: 'Female',
      location: 'Los Angeles',
      profilePhoto: 'assets/user_images/woman.png',
      userType: 'User',
      dateOfBirth: new Date('1997-08-20'),
      contact: 9876543210,
      countryCode: 91,
      height: 65
    },{
      username: 'Jane Smith8',
      age: 25,
      gender: 'Female',
      location: 'Los Angeles',
      profilePhoto: 'assets/user_images/woman.png',
      userType: 'User',
      dateOfBirth: new Date('1997-08-20'),
      contact: 9876543210,
      countryCode: 91,
      height: 65
    },{
      username: 'Jane Smith9',
      age: 25,
      gender: 'Female',
      location: 'Los Angeles',
      profilePhoto: 'assets/user_images/woman.png',
      userType: 'User',
      dateOfBirth: new Date('1997-08-20'),
      contact: 9876543210,
      countryCode: 91,
      height: 65
    },{
      username: 'Jane Smith10',
      age: 25,
      gender: 'Female',
      location: 'Los Angeles',
      profilePhoto: 'assets/user_images/woman.png',
      userType: 'User',
      dateOfBirth: new Date('1997-08-20'),
      contact: 9876543210,
      countryCode: 91,
      height: 65
    },
    {
      username: 'John Doe2',
      age: 30,
      gender: 'Male',
      location: 'New York',
      profilePhoto: 'assets/user_images/boy.png',
      userType: 'Admin',
      dateOfBirth: new Date('1992-05-15'),
      contact: 1234567890,
      countryCode: 1,
      height: 72
    },
    {
      username: 'John Doe3',
      age: 30,
      gender: 'Male',
      location: 'New York',
      profilePhoto: 'assets/user_images/boy.png',
      userType: 'Admin',
      dateOfBirth: new Date('1992-05-15'),
      contact: 1234567890,
      countryCode: 1,
      height: 72
    },
    {
      username: 'John Doe4',
      age: 30,
      gender: 'Male',
      location: 'New York',
      profilePhoto: 'assets/user_images/boy.png',
      userType: 'Admin',
      dateOfBirth: new Date('1992-05-15'),
      contact: 1234567890,
      countryCode: 1,
      height: 72
    },
    {
      username: 'John Doe5',
      age: 30,
      gender: 'Male',
      location: 'New York',
      profilePhoto: 'assets/user_images/boy.png',
      userType: 'Admin',
      dateOfBirth: new Date('1992-05-15'),
      contact: 1234567890,
      countryCode: 1,
      height: 72
    },
    {
      username: 'John Doe6',
      age: 30,
      gender: 'Male',
      location: 'New York',
      profilePhoto: 'assets/user_images/boy.png',
      userType: 'Admin',
      dateOfBirth: new Date('1992-05-15'),
      contact: 1234567890,
      countryCode: 1,
      height: 72
    },
  ];

  user_data: User = {
    username: "",
    age: 0,
    gender: "",
    location: "",
    profilePhoto: "assets/user_images/default_profile.png",
    userType: "User",
    dateOfBirth: new Date(),
    contact: 1234567890,
    countryCode: 91,
    height: 0
  };

  profile_count: number = this.users.length;
  isNewUser: boolean = true;
  isEditMode: boolean = true;

  // When is NewUser true then isEditMode always true
  // If newUser false then editmode can be false or true
  selectedUsername: string = "";

  dataError: { [key: string]: boolean } = {
    username: false,
    gender: false,
    age: false,
    location: false,
    profilePhoto: false,
    dateOfBirth: false,
    contact: false,
    countryCode: false,
    height: false
  };

  make_error_default() {
    this.dataError = {
      username: false,
      gender: false,
      age: false,
      location: false,
      profilePhoto: false,
      dateOfBirth: false,
      contact: false,
      countryCode: false,
      height: false
    };
  }


  make_user_data_default() {
    this.user_data = {
      username: "",
      age: 0,
      gender: "",
      location: "",
      profilePhoto: "assets/user_images/default_profile.png",
      userType: "User",
      dateOfBirth: new Date(),
      contact: 1234567890,
      countryCode: 91,
      height: 0,
    };
    this.profile_count = this.users.length;
    this.isNewUser = true;
    this.isEditMode = true;
  }

  handleAddUserClick() {
    this.make_user_data_default();
    this.make_error_default();
  }


  userGotSelected(username: string) {
    const selectedUser = this.users.find(user => user.username === username);
    if (selectedUser) {
      this.user_data = { ...selectedUser };
      this.isNewUser = false;
      this.isEditMode = false;
      this.selectedUsername = this.user_data.username;
      this.make_error_default();
    }
  }

  FormOptionClicked(optionAction: string) {
    switch (optionAction) {
      case 'Delete':
        this.deleteClick();
        break;
      case 'Save':
        this.saveClick();
        break;
      case 'Edit':
        this.editClick();
        break;
      default:
        alert("No action Taken");
        break;
    }
    this.selectedUsername = this.user_data.username;
  }

  checkIfAnyError(): boolean {
    for (const key in this.dataError) {
      // console.log("Key : "+ key + " : "+ this.dataError[key]);
      if (this.dataError[key]) {
        return true;
      }
    }
    return false;
  }

  saveClick() : void{
    this.make_error_default();
    this.checkUserDataValid();
    if (!this.checkIfAnyError()) { 
      if (this.isNewUser && this.isEditMode) {
        this.users.push({ ...this.user_data });
        this.make_user_data_default();
      }
      else if(!this.isNewUser && this.isEditMode) {
        console.log("aa")
        this.saveAfterEdit();
      }
    }
  }

  deleteClick(): void {
    this.make_error_default();
    if (this.user_data) {
      const index = this.users.findIndex(user => user.username === this.user_data.username);
      if (index >= 0) {
        this.users.splice(index, 1);
        this.make_user_data_default();
      }
      else {
        alert("Cannot Delete User");
      }
    }
  }

  editClick() {
    this.isNewUser = false;
    this.isEditMode = true;
    this.make_error_default();
  }

  saveAfterEdit() {
    console.log("HGi");
    if (this.user_data) {
      const index = this.users.findIndex(user => user.username === this.selectedUsername);
      if (index >= 0) {
        this.users[index] = { ...this.user_data };
        this.make_user_data_default();
      }
      else {
        alert("Cannot Find User To Edit");
      }
    }
  }

  checkUserDataValid(): void {
    const trimmedUsername = this.user_data.username.trim();
    let isUsernameUnique = this.users.every(user => user.username !== trimmedUsername);
    // console.log(this.user_data);
    if (!isUsernameUnique && trimmedUsername === this.selectedUsername) {
      isUsernameUnique = true;
    }
    if (trimmedUsername.length == 0 || !isUsernameUnique) {
      this.dataError['username'] = true;
    } else {
      this.dataError['username'] = false;
    }

    const isGenderValid = this.user_data.gender === 'Male' || this.user_data.gender === 'Female';
    if (!isGenderValid) {
      this.dataError['gender'] = true;
    } else {
      this.dataError['gender'] = false;
    }

    const isLocationValid = this.user_data.location ? this.user_data.location.trim().length > 0 : false;
    if (!isLocationValid) {
      this.dataError['location'] = true;
    } else {
      this.dataError['location'] = false;
    }

    const isProfilePhotoValid = this.user_data.profilePhoto ? this.user_data.profilePhoto.trim().length > 0 : false;
    if (!isProfilePhotoValid) {
      this.dataError['profilePhoto'] = true;
    } else {
      this.dataError['profilePhoto'] = false;
    }

    function isValidDate(date: Date): boolean {
      return date instanceof Date && !isNaN(date.getTime());
    }
    
    function isDateBeforeToday(date: Date): boolean {
      const today = new Date();
      today.setHours(0, 0, 0, 0);
      return date < today;
    }
    
    
    const isDateOfBirthValid = isValidDate(this.user_data.dateOfBirth) && isDateBeforeToday(this.user_data.dateOfBirth);
    // console.log(isDateOfBirthValid);
    // console.log(this.user_data.dateOfBirth);
    // console.log(isValidDate(this.user_data.dateOfBirth));
    // console.log(isDateBeforeToday(this.user_data.dateOfBirth));
    if (!isDateOfBirthValid) {
      this.dataError['dateOfBirth'] = true;
    } else {
      this.dataError['dateOfBirth'] = false;
    }

    const isContactValid = /^\d{10}$/.test(this.user_data.contact.toString());
    if (!isContactValid) {
      this.dataError['contact'] = true;
    } else {
      this.dataError['contact'] = false;
    }

    const isCountryCodeValid = /^\d+$/.test(this.user_data.countryCode.toString());
    if (!isCountryCodeValid) {
      this.dataError['countryCode'] = true;
    } else {
      this.dataError['countryCode'] = false;
    }

    const isHeightValid = this.user_data.height > 0;
    // console.log(isHeightValid);
    // console.log(this.user_data.height);
    if (!isHeightValid) {
      this.dataError['height'] = true;
    } else {
      this.dataError['height'] = false;
    }

  }





  ngOnInit(): void {
  }

}



