import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {
  // @Input() profile_count:number=0;
  @Output() addUserClick = new EventEmitter<void>();
  addUser(){
    this.addUserClick.emit();
  }
  constructor() { }

  ngOnInit(): void {
  }

}
