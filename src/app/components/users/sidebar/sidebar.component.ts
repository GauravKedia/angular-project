import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { User } from '../user_interface';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input() users: User[] = [];
  @Input() selectedUsername:string = "";
  @Output() userSelectedClicked = new EventEmitter<string>();

  searchInput: string = "";
  
  userGotSelected(username:string){
    this.userSelectedClicked.emit(username);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
