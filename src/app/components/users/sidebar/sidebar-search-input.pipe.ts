import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../user_interface';
@Pipe({
  name: 'sidebarSearchInput'
})
export class SidebarSearchInputPipe implements PipeTransform {

  transform(users: User[], searchInput:string): User[] {
    if (!searchInput) {
      return users;
    }
    const numericInput = parseInt(searchInput, 10);
    const isNumeric = !isNaN(numericInput);
    return users.filter((user: User) => {
      return user.username.toLowerCase().includes(searchInput.toLowerCase()) ||
        (isNumeric && user.age === numericInput) ||
        (user.userType.toLowerCase().includes(searchInput.toLowerCase())) ||
        // user.location.toLowerCase().includes(searchInput.toLowerCase()) ||
        user.gender.toLowerCase().includes(searchInput.toLowerCase());
    });
  }

}
